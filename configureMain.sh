
#########################################################
###### Configuration of the topology that we using ######
#########################################################
######################### ADHOC #########################
APnet=255.255.255.0

#Wi-Fi AP and ADHOC interface name.
ifADHOC=wlan1
ifAP=ap0
#essid name and channel for B.A.T.M.A.N. ad-hoc  nodes.
ESSID=adhoc-test
CHANNEL=1
#Cell ID of ad-hoc nodes.
CELLID=02:12:34:56:78:9A

declare -A AccesPointIPs

#Define the IPs for each node's Access Point subnet.
AccesPointIPs["node2-19"]=192.168.1.1
AccesPointIPs["node10-11"]=192.168.2.1
AccesPointIPs["node19-2"]=192.168.3.1
#echo ${AccesPointIPs[*]}
#########################################################
#########################################################
#########################################################




#########################################################
###################### Build Node #######################

#Find out which node is this.
HOST=$(hostname -s)
apIP=$(echo ${AccesPointIPs["$HOST"]})

modprobe ath5k
ifconfig $ifADHOC up
ifconfig $ifADHOC down
ifconfig $ifADHOC mtu 1532
iwconfig $ifADHOC mode ad-hoc essid $ESSID ap $CELLID channel $CHANNEL
iwconfig $ifADHOC txpower 1mW

modprobe batman-adv debug=2
batctl if add $ifADHOC
ifconfig $ifADHOC up
sysctl -w net.ipv4.ip_forward=1

ifconfig $ifAP 0.0.0.0

ip link add name br0 type bridge
ip link set dev $ifAP master br0
ip link set dev bat0 master br0

ip link set up dev $ifAP
ip link set up dev bat0
ip link set up dev br0

ip addr replace dev br0 $apIP/24


######################## ROUTING ########################

SUBNET=$(echo ${apIP} | cut -d"." -f1-3)
route del -net $SUBNET".0" netmask 255.255.255.0 br0
route add -net 192.168.0.0 netmask 255.255.0.0 br0


