MAIN="root@node19-2 root@node10-11 root@node2-19"

#run scripts to hosts
for node in $MAIN; do
    echo ""
    scp -o "StrictHostKeyChecking no" "configureMain.sh" ${node}:/root/
    ssh -o "StrictHostKeyChecking no" ${node} "bash configureMain.sh"
done
