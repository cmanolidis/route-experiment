HOSTS=node19-2.grid.orbit-lab.org,node10-11.grid.orbit-lab.org,node2-19.grid.orbit-lab.org
GROUP1=node1-19.grid.orbit-lab.org,node2-20.grid.orbit-lab.org,node3-19.grid.orbit-lab.org
GROUP2=node10-10.grid.orbit-lab.org,node11-10.grid.orbit-lab.org,node11-11.grid.orbit-lab.org
GROUP3=node18-2.grid.orbit-lab.org,node19-1.grid.orbit-lab.org,node20-2.grid.orbit-lab.org

omf load -i cmanolidis-node-node19-2.grid.orbit-lab.org-2016-07-01-04-03-35.ndz -t "$HOSTS,$GROUP1,$GROUP2,$GROUP3"
omf tell -a on -t "$HOSTS","$GROUP1","$GROUP2","$GROUP3"

# omf tell -a reset -t "$HOSTS","$GROUP1","$GROUP2","$GROUP3"
