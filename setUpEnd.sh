#load and run scripts on each end node
for dest in $(<endNodes.txt); do
    echo ""
    scp -o "StrictHostKeyChecking no" configureEnd.sh $"root@"${dest}:/root/
    ssh -o "StrictHostKeyChecking no" $"root@"${dest} "bash configureEnd.sh"
done
