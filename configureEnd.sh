NETMASK=255.255.255.0
#Wi-Fi interface name.
IFACE=wlan0
#The wireless key for the Access Point that will be used.
KEY=SECRETPASSWORD


declare -A ipARRAY
declare -A essidARRAY

#########################################################
###### Configuration of the topology that we using ######
#########################################################

# Define IPs for nodes of SUBNET 1.
ipARRAY["node1-19"]=192.168.1.2
ipARRAY["node2-20"]=192.168.1.3
ipARRAY["node3-19"]=192.168.1.4

# Define IPs for nodes of SUBNET 2
ipARRAY["node10-10"]=192.168.2.2
ipARRAY["node11-10"]=192.168.2.3
ipARRAY["node11-11"]=192.168.2.4

# Define IPs for nodes of SUBNET 3
ipARRAY["node18-2"]=192.168.3.2
ipARRAY["node19-1"]=192.168.3.3
ipARRAY["node20-2"]=192.168.3.4

#echo ${ipARRAY[*]}

#Define the AP essid of each subnetwork.
essidARRAY["192.168.1"]=node2-19
essidARRAY["192.168.2"]=node10-11
essidARRAY["192.168.3"]=node19-2
#########################################################


#Find out what node this is.
HOST=$(hostname -s)

#Get the IP of this node.
IP=$(echo ${ipARRAY["$HOST"]})

#Get the subnet ip of this node.
SUBNET=$(echo ${ipARRAY["$HOST"]} | cut -d"." -f1-3)

#Get the essid for the corresponding AP of this node.
APessid=$(echo ${essidARRAY["$SUBNET"]})

#Get the ip of this subnet's the gateway.
GATEWAY=$SUBNET".1"

#Choose the appropriate wireless card for this node.
modprobe ath9k
ARG=$(echo $(ifconfig $IFACE) | awk '{print$1;}')
if ! [ "$ARG" == $IFACE ]; then
    echo "ath9k does not supported we use: ath5k"
    modprobe ath5k
fi


#IFSUB=$(ifconfig $IFACE | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*')

#Verify that node is not already setup and setup node.
if ! [[ $(echo $(ifconfig $IFACE | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*') | cut -d"." -f1-3) == $SUBNET ]]; then

    echo "IP $IP will be assigned to $HOST."
    echo "The gateway of subnet $SUBNET".0" is: $APessid"

    #Setting up node's IP and connecting to subnet's AP.
    iwconfig $IFACE txpower 1mW
    ifconfig $IFACE $IP netmask $NETMASK up
    wpa_passphrase $APessid $KEY > wpa.conf
    wpa_supplicant -i$IFACE -cwpa.conf -B

    #Set up gateway
    route add -net 192.168.0.0/16 gw $SUBNET".1" dev $IFACE
    route del -net $SUBNET".0" netmask 255.255.255.0 $IFACE

else
    echo "$HOST already configured"
fi

